<?php

// Empieza aquí a escribir tu código PHP...
$n = $_GET["num"]; //Se obtiene el valor

function piramide($num) {//Función
    $out = "";//Variable de output
    for($i = 1; $i <= $num; $i++){//Se pinta cada línea
        $ex = str_repeat(" ",$num-$i);//Primero tantos espacios como el número total de filas menos la fila
        $b = "*".str_repeat(" *",$i-1);//Luego un asterisco y tantos " *" como número de asteriscos en la fila menos 1
        $out .= "<pre>".$ex.$b."</pre>";//Se une todo en la línea
    }
    return $out;//Se devuelve el resultado
}

echo piramide($n);