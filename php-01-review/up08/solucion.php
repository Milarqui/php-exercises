<?php

$cadenas = ["patata", "cebolla", "sal", "pimienta", "te", "agua","a","esternocleido"];

// Continua...
$min = 1000;//Inicializamos un valor máximo muy bajo y mínimo muy alto
$max = 0;
foreach($cadenas as $cosa) {//Para cada palabra de la cadena
    $l = strlen($cosa); //Calcula la longitud de la palabra
    if($l>$max){ // Si la longitud es mayor que el máximo, cambia el máximo
        $max = $l;
    }
    if($l<$min){ // Si la longitud es menor que el mínimo, cambia el mínimo
        $min = $l;
    }
}
echo "El string más corto es $min y el string más largo es $max";