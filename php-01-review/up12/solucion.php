<?php

// Empieza aquí a escribir tu código PHP...

function factorial($n){// Función  para calcular el factorial
    if($n <= 1){// Si n es menor o igual a 1, devuelve 1
        return 1;
    }
    else{//Si n es mayor que 1, devuelve el factorial de n-1 multiplicado por n
        return factorial($n-1)*$n;
    }
}

$num = $_GET["num"];
print_r("El factorial de $num es ".factorial($num));