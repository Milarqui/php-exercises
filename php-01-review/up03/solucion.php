<?php

// Empieza aquí a escribir tu código PHP...

$r = $_GET["valor"]; //Obtiene el valor que se quiere comprobar

function elevado($num) {// Función para determinar a qué factor se tiene que elevar un número para que supere 10000
    if($num <= 1){ // Si el valor es menor o igual a 1 no puede elevarse hasta 10000, por lo que es infinito
        return "infinito";
    }
    else { // En los demás casos
        $n = 0; // Inicializamos a n=0 y v=1 (ya que cualquier número elevado a 0 es 1)
        $v = 1;
        do { // Bucle do-while que funcione hasta que detecte de v > 10000
            $v = $v * $num; //Multiplica la variable $v por el número
            $n++; // Incrementa el contador (num ^ n = v)
            echo "$num ^ $n = $v<br>"; // Imprime en pantalla el resultado parcial
        } while($v <= 10000);
        return $n; // Devuelve el contador final
    }
}
$n = elevado($r);
echo "$r debe ser elevado a $n para superar 10000";