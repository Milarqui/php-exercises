<?php

// Empieza aquí a escribir tu código PHP...

function archivos($filearr) { //Función, se le pasa un array como input
    foreach($filearr as $i => $file) {//Para cada nombre de archivo se coge también la clave
        $name = explode(".",$file); //Separa el nombre entre nombre y extensión
        if($name[1] == "exe") { //Comprueba si la extensión es exe
            $name[0] = strtoupper($name[0]); //En ese caso, pone el nombre en mayúsculas
        }
        else if($name[1] == "db") {//Comprueba si la extensión es db
            $name[0] = strtolower($name[0]); //En ese caso, pone el nombre en minúsculas
        }
        $f = implode('.',$name); //Vuelve a formar el nombre completo del archivo tras la transformación
        $filearr[$i] = $f; //Cambia el valor en el array
    }
    return $filearr; //Devuelve el array cambiado
}

$farr = ["Mina.rar", "Ochaco.exe", "Momo.db", "Izuku.exe", "Eijiro.zip", "Tenya.db"];

print_r($farr);
print_r("<br>");
print_r(archivos($farr));