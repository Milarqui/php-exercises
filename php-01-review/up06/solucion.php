<?php

$colores = ['blanco', 'verde', 'rojo'];

// Empieza aquí a escribir tu código PHP...

print_r(implode(", ",$colores)); //Une los elementos del array usando ", " como pegamento y los muestra por pantalla

natcasesort($colores); //Ordena alfabéticamente los elementos del array ignorando mayúsculas y minúsculas

print_r("<ul><li>".implode("</li><li>",$colores)."</li></ul>"); //Une el array ordenado de forma que parezca una lista