<?php

// Empieza aquí a escribir tu código PHP...

function milenializar($texto){//Función para cambiar el texto
    $acentos = array("Á","É","Í","Ó","Ú","á","é","í","ó","ú","ñ");
    $vocales = array("A","E","I","O","U","a","e","i","o","u","Ñ");
    $text_sin_a  = str_replace($acentos,$vocales,$texto);//Quitamos los acentos y transformamos las ñ en Ñ
    $TEXT = strtoupper($text_sin_a);//Se transforma todo a mayúsculas
    $previous = ["IGUAL","PORQUE","QUE","¿","¡","GU","BU"];
    $after =    ["="    ,"XQ"    ,"K"  ,"" ,"" ,"W" ,"W"];
    return str_replace($previous,$after,$TEXT);//Se reemplazan todos los elementos
}

$texto = "Durante el siglo diecinueve, el guano fue uno de los materiales estratégicos más importantes para la industria bélica, al tratarse de una de las pocas fuentes naturales de nitratos accesibles por el hombre. ¡Incluso guerras se han librado en la lucha por él! ¿Quién habría dicho que el excremento de animal podía ser tan importante? Bueno, igual los antepasados del hombre se extrañarían de ver tan extraño comportamiento en ese momento...";

echo milenializar($texto);