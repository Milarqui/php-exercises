<?php

// Empieza aquí a escribir tu código PHP...

$fTexto = fopen("texto.txt","r"); //Abre el archivo de texto a leer

$i = 0; //Inicia el contador de líneas
while(($line = fgets($fTexto)) !== false) { //Hasta llegar al final del archivo
    $i++; //Aumenta el número de líneas
    $words = explode(" ",$line); //Separa el texto de la línea en palabras usando el marcador de los espacios
    $n = count($words); //Cuenta cuantas palabras hay en la línea
    echo "LÍNEA $i ($n palabras): ".$line."<br>"; //Imprime por pantalla la línea, el número de palabras y la línea
}