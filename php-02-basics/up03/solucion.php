<?php

// Empieza aquí a escribir tu código PHP...
$acentos = array("Á","É","Í","Ó","Ú");
$vocales = array("á","é","í","ó","ú");

$palabra = $_POST["palabra"];//Coge la palabra a buscar
$pal = strtolower($palabra); //Se pone la palabra en minúsculas
$pal = str_replace($acentos,$vocales,$pal); //Se cambian los acentos para que estén en minúsculas
$fichero = $_FILES["fichero"]["tmp_name"];//Se coge el nombre del fichero
$file = file_get_contents($fichero);//Se obtiene el contenido del fichero
$lines = strtolower($file);//Se pone todo el texto en minúsculas
$lines = str_replace($acentos,$vocales,$lines);//Se ponen los acentos en minúsculas

$num = substr_count($lines,$pal);//Se cuenta cuantas veces está la palabra pal dentro del texto

echo "La palabra '$palabra' aparece $num veces";