<?php

require_once("ElementoMultimedia.php");

class Capitulo{
    var $numero;
    var $titulo;
    var $estreno;
    var $descripcion;
    var $duracion;
    var $valoracion;
    var $num_val;
    var $video;
    var $portada;
    function __construct($numero,$nombre,$fecha,$time,$url_v,$descripcion="",$url_f="")
    {
        $this->numero = $numero;
        $this->titulo = $nombre;
        $this->estreno = $fecha;
        $this->descripcion = $descripcion;
        $this->duracion = $time;
        $this->valoracion = 0.0;
        $this->num_val = 0;
        $this->video = new ElementoMultimedia($url_v,"Video");
        $this->portada = new ElementoMultimedia($url_f,"Portada");
    }
    function valorar($value)
    {
        $n = $this->num_val;
        $v = $this->valoración;
        $this->num_val = $n+1; 
        $this->valoracion = ($v*$n+$value)/($n+1);
    }
}