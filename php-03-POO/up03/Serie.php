<?php

require_once("Temporada.php");

class Serie{
    var $nombre;
    var $genero;
    var $temporadas;
    var $descripcion;
    function __construct($nombre,$genero="",$descripcion="",$temp_inicial="")
    {
        $this->nombre = $nombre;
        $this->genero = $genero;
        $this->descripcion = $descripcion;
        $this->temporadas = [];
        if($temp_inicial != "")
        {
            $year = $temp_inicial['anno'];
            new_season($year);
        }
    }

    function new_season($year)
    {
        $n = count($this->temporadas)+1;
        $t = new Temporada($n,$year);
        array_push($this->temporadas,$t);
    }

    function cap_temp($n)
    {
        $temporada = $this->temporadas[$n];
        $l = count($temporada->capitulos);
        return $l;
    }

    function valoracion_media()
    {
        $v = 0;
        $n = 0;
        foreach($this->temporadas as $temporada)
        {
            $values = $temporada->val_media();
            $v += $values[0];
            $n += $values[1];
        }
        if($n == 0)
        {
            return 0;
        }
        else
        {
            return $v/$n;
        }
    }
}