<?php

require_once("Capitulo.php");

class Temporada{
    var $numero;
    var $anno;
    var $capitulos;
    function __construct($num,$a,$cap_inicial = "")
    {
        $this->numero = $num;
        $this->anno = $a;
        $this->capitulos = [];
        if($cap_inicial != "")
        {
            $name = $cap_inicial['nombre'];
            $date = $cap_inicial['fecha'];
            $time = $cap_inicial['duracion'];
            $url = $cap_inicial['url'];
            $descripcion = $cap_inicial['descripcion'];
            new_chapter($name,$date,$time,$url,$descripcion);
        }
    }

    function new_chapter($name,$date,$time,$url,$descripcion="")
    {
        $n = count($this->capitulos)+1;
        $chapter = new Capitulo($n,$name,$date,$time,$url,$descripcion);
        array_push($this->capitulos,$chapter);
    }

    function val_media()
    {
        $v = 0;
        $n = 0;
        foreach($this->capitulos as $chapter)
        {
            $v += $chapter->valoracion;
            $n += $chapter->num_val;
        }
        if($n == 0)
        {
            return [0,0];
        }
        else
        {
            return [$v/$n,$n];
        }
    }
}