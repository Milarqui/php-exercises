<?php

require_once("passwordValidator.php");
$password = $_GET["password"];
$contraseña = new passwordValidator($password);
$safety = ["Nulo","Bajo","Medio","Alto"];
if($contraseña->isValid()){
    $val = $safety[$contraseña->seguridad()];
    echo "La contraseña es válida, Nivel de seguridad $val";
}
else{
    echo "La contraseña no es válida";
}