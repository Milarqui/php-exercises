<?php
class passwordValidator{
    var $password;
    function __construct($pwd) {
        $this->password = $pwd;
    }
    function isValid(){
        $pwd = $this->password;
        $l = strlen($pwd);
        $arraypoint = [".",",","-","_",";"];
        $punctuation = false;
        foreach($arraypoint as $point){
            $v = substr_count($pwd,$point);
            if($v > 0){
                $punctuation = true;
            }
        }
        if($l >= 8 and $punctuation){
            return true;
        }
        else {
            return false;
        }
    }
    function seguridad(){
        $pwd = $this->password;
        $l = strlen($pwd);
        $level = 0;
        $arrpoint = [".",",","-","_",";"];
        $arrnum = ['0','1','2','3','4','5','6','7','8','9',];
        if($this->isValid()){
            $level = 1;
            $mayus = strtoupper($pwd);
            $minus = strtolower($pwd);
            if($l >= 12 and $mayus != $pwd and $minus != $pwd){
                $level = 2;
                $v = 0;
                foreach($arrnum as $num){
                    $v += substr_count($pwd,$num);
                }
                $p = 0;
                foreach($arrpoint as $pnt){
                    $p += substr_count($pwd,$pnt);
                }
                if($l >= 16 and $v>0 and $p>1){
                    $level = 3;
                }
            }
        }
        return $level;
    }
}