<?php

require_once("Cuadrado.php");
require_once("Rectangulo.php");
require_once("Circulo.php");

$cuadrado1 = new Cuadrado("6");
echo "EL AREA DEL CUADRADO ES: " . $cuadrado1->calcularArea() . "<br>";

$rectangulo1 = new Rectangulo(3, 4);
echo "EL AREA DEL RECTANGULO ES: " . $rectangulo1->calcularArea() . "<br>";

$circulo1 = new Circulo(1/sqrt(M_PI));
echo "EL AREA DEL CIRCULO ES: " . $circulo1->calcularArea() . "<br>";