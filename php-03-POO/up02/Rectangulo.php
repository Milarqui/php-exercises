<?php

class Rectangulo extends Poligono{
    protected $a,$b;
    public function __construct($a,$b)
    {
         $this->a = $a;
         $this->b = $b;
    }
    public function calcularArea(){
        return $this->a*$this->b;
    }
}