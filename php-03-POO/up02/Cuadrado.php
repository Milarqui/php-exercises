<?php

require_once("Poligono.php");

class Cuadrado extends Poligono{
    protected $l;
    public function __construct($l)
    {
         $this->l = $l;       
    }
    public function calcularArea(){
        return pow($this->l,2);
    }
}