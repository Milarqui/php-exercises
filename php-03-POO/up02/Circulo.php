<?php

class Circulo extends Poligono{
    protected $r;
    public function __construct($r)
    {
         $this->r = $r;       
    }
    public function calcularArea(){
        return 4*M_PI*pow($this->r,2);
    }
}