<?php
    $library = file_get_contents("cineteca.json");
    $peliculas = json_decode($library);
    echo "<h1>Lista de películas</h1>";
    echo "<table>
        <tr>
            <td>Película</td>
            <td>Género</td>
            <td>Año</td>
            <td>Director</td>
            <td>Borrar</td>
            <td>Editar</td>
        </tr>";
    foreach($peliculas as $pelicula){
        $name=$pelicula->titulo;
        $gen=$pelicula->genero;
        $a=$pelicula->anno;
        $dir=$pelicula->director;
        echo "<tr id=$name>
            <td>$name</td>
            <td>$gen</td>
            <td>$a</td>
            <td>$dir</td>
            <td><input type='button' class='Borrar' id=$name value='X'></td>
            <td><input type='button' class='Editar' id=$name value='X'></td>
            </tr><br>";
    }
    echo "</table>"
?>