<?php

if(!file_exists("cineteca.json")){
    $peliculas = [];
}
else {
    $contenido = file_get_contents("cineteca.json");
    if($contenido == ""){
        $peliculas = [];
    }
    else{
        $peliculas = json_decode($contenido,true);
    }
}
$cineteca = fopen("cineteca.json","w");

$peli = [
    "titulo"=> $_POST["titulo"],
    "genero"=> $_POST["genero"],
    "anno"=> $_POST["anno"],
    "director"=> $_POST["director"]
];
array_push($peliculas,$peli);
fwrite($cineteca,json_encode($peliculas));
header("LOCATION:formulario.html");